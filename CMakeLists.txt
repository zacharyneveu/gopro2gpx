﻿cmake_minimum_required (VERSION 3.22)

project(gopro2gpx)

add_subdirectory(gpmf-parser)

add_executable (gopro2gpx 
  src/main.c 
  src/GPMF_mp4reader.c 
  src/gpx.c 
  src/utils.c 
  src/list.c 
  src/platform.c 
  src/mystring.c 
  src/goprofilenames.c 
  src/gpmf.c 
)

target_include_directories(gopro2gpx PRIVATE ${CMAKE_SOURCE_DIR} gpmf-parser)
target_link_libraries(gopro2gpx PRIVATE m gpmf)

if(WIN32)
  target_compile_definitions(gopro2gpx _FILE_OFFSET_BITS=64)
endif()

install(TARGETS gopro2gpx)